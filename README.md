# linkat-servidor-nginx
This project is maintained by [Linkat](http://linkat.xtec.cat).
In this repository there are provided the files for building a docker image for running nginx with the basic web of Linkat-servidor

## DockerHub
You can find the docker image releases at https://hub.docker.com/r/linkat/linkat-servidor-nginx

## Repository
Public repository in GitLab: https://gitlab.com/linkatedu/linkat-servidor-nginx

## Build
The [Dockerfile](https://gitlab.com/linkatedu/fogproject/blob/master/Dockerfile) file defines all needed for building the image. It can be built with:
```
VERSION=0.0.1
docker build -t linkat/linkat-servidor-nginx:$VERSION .
```

## Run

```
docker run -p 80:80 --name linkat-servidor-nginx -d linkat/linkat-servidor-nginx
```

If you want to mount a volume data for diferent web content, you can use:

```
docker run -p 80:80 -v /some/web/content:/usr/share/nginx/html --name linkat-servidor-nginx -d linkat/linkat-servidor-nginx:1.0.0
```

